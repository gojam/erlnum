# Erlang String Parser

For those occasions where Erlang is just printing a list of numbers
and you really want to see the ASCII equivalent, use **erlnum**.

## Usage

    erlnum file.txt
    cat file.txt erlnum

## Example

```
$ cat input.txt
72,101,108,108,111,10
87,111,114,108,100,10

$ cat input.txt | erlnum
Hello<10>World<10>
```

## Install

Checkout the code and simply:

    go install
    